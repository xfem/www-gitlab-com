---
layout: handbook-page-toc
title: "Hacker News"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Hacker News is an important social channel. Threads that mention GitLab's structure, values, product vision, or other sensitive blog posts, articles, etc. should be treated as important, while posts about GitLab that land on the front page of Hacker News should be treated as both important and urgent.

GitLab mentions on Hacker News are tracked on the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel. Posts about GitLab that land on the front page of Hacker News generate a notification that is shared in [#community-relations](https://gitlab.slack.com/messages/community-relations).

## What performs well 

In 2020, we ran an audit to see what types of posts landed on the front page of Hacker News. We learned that the following types of GitLab-related content generated the most interest on Hacker News: 
- Major company news such as announcements about funding, acquisitions, or moving features to core 
- Release posts 
- Announcements from our open source program members including posts from Gnome, KDE, and WikiMedia and other open-source focused content 
- Technical blog posts
- Remote-focused content including handbook pages and the Remote Work report 

When considering what types of content to publish on social media, these are all good types of posts and pages to share on Twitter, LinkedIn, relevant Slack communities, and other social channels. 

## Response workflow

1. When alerted by the "Hacker News Front Page Bot" that an article referencing GitLab is on the Front Page of Hacker News, a Developer Evangelists should coordinate to ensure a DRI is assigned to the post to review, monitor the comments, and respond accordingly. 
1. Developer Evangelists should also review the #hn-mentions Slack channel 1-2 times per day for mentions that require a response. If you are able to provide a quick response/resource, reply on [news.ycombinator.com](https://news.ycombinator.com) using your personal Hacker News account and indicate that you replied by leaving a :white_check_mark: on the Slack comment. _Note that [the window to edit a comment is 2 hours](https://github.com/minimaxir/hacker-news-undocumented#editdelete-time-limits), afterwards you cannot edit or delete a Hacker News comment._
1. If necessary, you may also wish to share comments with relevant experts who may be able to provide more detailed or insightful comments. This can be done by sharing relevant posts or comments in an appropriate Slack channel if you judge additional input is required.

## Best practices when responding on Hacker News

When responding to a post about GitLab on Hacker News:

- Don't post answers that are almost the same, link to the original one instead.
- Address multi-faceted comments by breaking them down and using points, numbering and quoting.
- When someone posts a Hacker News thread link, monitor that thread manually. Don't wait for the notifications in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, because sometimes they're delayed by a few hours.

## Social media guidelines

- Never submit GitLab content to Hacker News. Submission gets more credibility if a non-GitLab Hacker News community member posts it, we should focus on making our posts interesting instead of on submitting it.
- Don't share links to Hacker News stories/comments on Slack or Twitter and ask others to upvote because it might set off the voting detector.
- Don't make the first comment on a Hacker post, allow people to leave comments and ask questions.
- Avoid using corporate jargon like 'PeopleOps'.
- Always address the Hacker News community as peers. Be sure to always be modest and grateful in responses.
- If you comment yourself make sure it is interesting and relevant.
- Make yourself familiar with [A List of Hacker News's Undocumented Features and Behaviors](https://github.com/minimaxir/hacker-news-undocumented) to understand Hacker News behaviour and moderation rules.
- Check the tone of your response: Don’t be defensive, but instead share your point of view. 
- Try to teach people something interesting they didn’t know already.
- Add value to your post with data points or direct links.

Note: You can find the full list of social media guidelines [here](/handbook/marketing/social-media-guidelines/)

## Automation

Hacker News mentions are piped to Community Zendesk and the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel by [Zapier](https://zapier.com).
