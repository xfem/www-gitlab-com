---
layout: secure_and_protect_direction
title: "Category Direction - Security Orchestration"
description: "GitLab's Security Orchestration category provides unified policy and alert orchestration capabilities that span across the breadth of GitLab's security offerings."
canonical_path: "/direction/protect/security_orchestration/"
---

- TOC
{:toc}

## Protect

| | |
| --- | --- |
| Stage | [Protect](/direction/protect) |
| Maturity | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-12-08` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Security Orchestration in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute. We welcome feedback, bug reports, feature requests, and community contributions.

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecurity%20Orchestration) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/822) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for container security, we'd especially love to hear from you.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::protect" ~"Category:Security Orchestration" ~"group::container security"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

We believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview
Security Orchestration is an overlay category that provides policy and alert orchestration support for all the scanners and technologies used by GitLab's Secure and Protect stages.  The goal is to provide a single, unified user experience that is consistent and intuitive.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
1. [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
1. [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
1. [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#alex-security-operations-engineer)
1. [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
The work done in the Security Orchestration category will address a wide range of use cases.  To maintain focus, some of the higher priority problems we aim to solve are as follows:

| Problem | Solution |
| ------- | -------- |
| Organizations with large numbers of projects have no way to centrally manage when scans are run | Allow policies that require scans to be run to be created at the group and instance levels |
| Technical knowledge of Gitlab’s yaml parameters is required to use Secure features | Give users flexibility to edit policies via yaml or via a user friendly rules editor |
| GitLab does not have a place to review Security Alerts in the UI | Create a new Alert dashboard dedicated to security alerts |

Throughout our work in the Security Orchestration category, there are a few guiding principles that drive our designs. Not all of these capabilities will be available in our initial release; however, these principles guide where we are headed and where we want to end up in the long term.

1. All policy management should be capable of being done in natural English language sentences for ease of understandability
1. All policies will translate down to code (yaml or otherwise) and will be editable as code for technical users
1. Policy changes will need to support a two-step approval process so no one individual can edit policies unilaterally
1. Policy changes will all need to be audited
1. Policies will leverage Workspaces where possible to avoid duplicating work between the group and instance policy levels

#### What is our Vision (Long-term Roadmap)

Although this category is new, our vision is broad.  We plan to support both scan schedule and scan results policies for all of GitLab's scanners at the Instance, Group, and Project levels.  In addition, we intend to add full support for two-step approvals and proper audit trails of any changes made.  In the long-run, we intend to provide visibility into the impact (blast radius) a policy will have before it is deployed, add support for complex orchestration policies, and auto-suggest policies based on your unique environment and codebase.  Policy changes will need to be fully audited and optionally run through a two-step approval process.

Beyond policies, we also intend to add support for an Alert workflow that will allow users to be notified of events that require manual, human review to determine the next steps to take.  This workflow will eventually support auto-suggested responses and recommended changes to policies to reduce false positives and automate responses whenever possible.

The matrixes below describe the scope of the work that is planned in the long-run for the Security Orchestration category as well as our progress toward the end goal.  Eventually we would like to have support for most, if not all of the boxes in the tables below.

![Security Orchestration Tables](/images/direction/protect/security-orchestration-matrixes.png)

#### What's Next & Why (Near-term Roadmap)
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
We are planning to start by adding support for managing [DAST scan schedule policies](https://gitlab.com/groups/gitlab-org/-/epics/4598) at the project level.  This initial work will allow us to build a framework that we can then extend to other scanners, and also extend to the group and instance levels.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->
To be determined

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
[Planned to Minimal](https://gitlab.com/groups/gitlab-org/-/epics/4594)

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
We plan to measure the success of this category based on the total number of users that interact with the Policy and Alert pages in the GitLab UI.

## Competitive Landscape

As this category is new, we are still completing our evaluation of the competitive landscape.

## Analyst Landscape

As this category is new, we have not yet engaged analysts on this topic.
